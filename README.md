# PHP for Docker developement

**Currently : PHP 8.0.x**

This build is geared towards developing on WordPress and Laravel. The base Docker file was taken from [WordPress php8.0-fpm-alpine](https://github.com/docker-library/wordpress/blob/b9af6087524edc719249f590940b34ef107c95ca/latest/php8.0/fpm-alpine/Dockerfile).

## Getting started

Build the image :

```
docker build -t php80 .
```

Run a command :

```
docker run -it php80 /bin/bash
```

## Key Software

- php8.0
- composer
- pdo_mysql (added for laravel)
- wp-cli

## Becuase I forget

```
docker build -t php80 .

echo $password | docker login --username=username --password-stdin

docker tag php80 username/php80

docker push username/php80
```

## Whats next?

This is all a bit static. When the version we need goes up to 8.1 it might not make sense to update this repo. I think I need to learn about tagging in Docker more.
